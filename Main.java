public class Main {
    public static void main(String[] args) {
        Node [] firstLayer;
        Node [] secondLayer;
        firstLayer = new Node[6];
        secondLayer = new Node[4];
        for (int i = 0; i < firstLayer.length; i++) {
            Node e = new Node();
            firstLayer[i]=e;
        }
        for (int i = 0; i < secondLayer.length; i++) {
            Node e = new Node();
            secondLayer[i]=e;
        }
        Weight[] firstSecond = new Weight[24];
        for (int i = 0; i < firstLayer.length; i++) {
            for (int j = 0; j < secondLayer.length; j++) {
                Weight e = new Weight();
                e.launch= firstLayer[i];
                e.finish=secondLayer[j];
                e.weight=0.5;
                firstSecond[j]=e;
            }
        }
        firstLayer[0].output=10;
        firstLayer[1].output=20;
        firstLayer[2].output=15;
        firstLayer[3].output=30;
        firstLayer[4].output=60;
        firstLayer[5].output=200;
        LibOfComToAI.InitializationLayer(firstSecond,secondLayer);
        for (int i = 0; i < secondLayer.length; i++) {
            System.out.println(secondLayer[i]);
        }
    }
}