public class LibOfComToAI {
    public static void WeightingLayer(Weight[] weights){
        for (int i = 0; i < weights.length; i++) {
            weights[i].Weighting();
        }
    }
    public static void ActivationLayer(Node[] nodes){
        for (int i = 0; i < nodes.length; i++) {
            nodes[i].Activation();
        }
    }
    public static void InitializationLayer(Weight[] incomingWeights,Node[] nodes){
        WeightingLayer(incomingWeights);
        ActivationLayer(nodes);
    }

}
